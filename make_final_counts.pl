#!/usr/bin/perl
use strict;
use autodie;
use lib '/home/simroux/Utiles/Perl_Modules/';
use Custom::Utils;
use Getopt::Long;
my $h='';
my $in_file='';
GetOptions ('help' => \$h, 'h' => \$h, 'f=s'=>\$in_file);
if ($h==1 || $in_file eq ""){ # If asked for help or did not set up any argument
	print "# Script to get the final counts
# Arguments : 
# -f : in file (initial counts, e.g. Gene_counts/Gene_count_syn_control.csv)\n";
	die "\n";
}

# Checking the rRNA in the Syn genome
my %check_rRNA;
my $file="Gene_Table_Host.csv";
open my $csv,"<",$file;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tab[1] eq "rRNA"){$check_rRNA{$tab[0]}=1;}
}
close $csv;


my @list=($in_file);
my $unm_file=$in_file;
$unm_file=~s/\.csv/-unmapped.csv/;
if ($unm_file eq $in_file){
	die("$unm_file is not right ... it's the same as $in_file\n");
}
# my @list=($unm_file);
push(@list,$unm_file);

my $out_file=$in_file;
$out_file=~s/\.csv/-final.csv/;
if ($out_file eq $in_file){
	die("$out_file is not right ... it's the same as $in_file\n");
}


# Get libs info
print "Gathering libraries information ...\n";
my %info;
my %name_to_id;
my $j=1;
my @tab_f=<Libraries_Id/*.txt>;
foreach my $file (@tab_f){
	open my $tsv,"<",$file;
	my %c_name;
	my $tag=0;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^1=libraryName/){
			$tag=1;
			my @tab=split("\t",$_);
			for (my $i=0;$i<=$#tab;$i++){
				if ($tab[$i]=~/\d+=(.*)/){
					$c_name{$i}=$1;
				}
				else{
					print "Pblm with $tab[$i]\n";
				}
			}
		}
		elsif($tag==1){
			my @tab=split("\t",$_);
			$info{$tab[0]}{"order"}+=$j;
			$j++;
			for (my $i=1;$i<=$#tab;$i++){
				$tab[$i]=~s/\s/\_/g;
				$info{$tab[0]}{$c_name{$i}}=$tab[$i];
				if ($c_name{$i} eq "sampleName"){
					$name_to_id{$tab[$i]}=$tab[0];
					if ($tab[$i]=~/.*(\d)([A-D])/){
						$info{$tab[0]}{"time_point"}=$1;
						$info{$tab[0]}{"replicate"}=$2;
					}
				}
			}
		}
	}
	close $tsv;
}


my %store;
my %c_name;
my %check_sample;
my %count;
foreach my $file (@list){
	my $tag=0;
	%c_name=();
	print "Reading $file\n";
	open my $csv,"<",$file;
	while(<$csv>){
		chomp($_);
		my @tab=split(",",$_);
		if ($tag==0){
			$tag=1;
			for (my $i=1;$i<=$#tab;$i++){
				$check_sample{$tab[$i]}=1;
				$c_name{$i}=$tab[$i];
			}
		}
		else{
			for (my $i=1;$i<=$#tab;$i++){
				$store{$tab[0]}{$c_name{$i}}+=$tab[$i];
				if ($check_rRNA{$tab[0]}==1){$count{$c_name{$i}}{"RNA"}+=$tab[$i];}
				else{$count{$c_name{$i}}{"other"}+=$tab[$i];}
			}
		}
	}
	close $csv;
}
print "## Sample,Time_point,Replicate,Total_Filtered_Reads,RNA,Other_genes\n";
foreach my $sample (sort keys %count){
	print "$sample,$info{$name_to_id{$sample}}{time_point},$info{$name_to_id{$sample}}{replicate},$info{$name_to_id{$sample}}{filteredReads},$count{$sample}{RNA},$count{$sample}{other}\n";
}

open my $s1,">",$out_file;
my @tab_sample=sort keys %check_sample;
my $f_l="Gene,".join(",",@tab_sample);
print $s1 "$f_l\n";
foreach my $gene (sort keys %store){
	my $line=$gene;
	foreach my $sample (@tab_sample){
		if (defined($store{$gene}{$sample})){$line.=",".$store{$gene}{$sample}}
		else{$line.=",0";}
	}
	print $s1 "$line\n";
}
close $s1;