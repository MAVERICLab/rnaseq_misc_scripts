#!/usr/bin/perl
use strict;
use Getopt::Long;
my $h='';
my $in_dir='';
my $out_file='';
GetOptions ('help' => \$h, 'h' => \$h,'d=s'=>\$in_dir,'o=s'=>\$out_file);

if ($h==1 || $in_dir eq "" || $out_file eq ""){ # If asked for help or did not set up any argument
	print "# Script to count all reads (raw and QC) in all samples
# Arguments : 
# -d --d : input dir
# -o --o : output file (tsv)\n";
	die "\n";	
}
my @tab_1=<$in_dir/*fastq.gz>;

open my $s1,">",$out_file;
foreach my $file (@tab_1){
	$file=~/.*\/([^\/]+)\.fastq.gz/;
	my $code=$1;
	print "Processing $file -> $code ...\n";
	open my $fq,"gunzip -c $file |";
	my $tag=0;
	my $c_n=0;
	my $c_bp=0;
	my $c_n_seq=0;
	while(<$fq>){
		chomp($_);
		# Old version of the Illumina FastQ header line
		if ($_=~/^\@(.+:.+:\d:\d+:\d+:\d+\/\d)$/){
			$c_n++;
			$tag=1;
		}
		# New version of the ILlumina FastQ Header line
		elsif($_=~/^\@(.+:\d+:[^:]+:\d+:\d+:\d+:\d+)\s\d:[YN]:\d+:[^:]+$/){
			$c_n++;
			$tag=1;
		}
		elsif($_=~/^\@\d+_\d+/){
			$c_n++;
			$tag=1;
		}
		elsif($tag==1){
			$c_bp+=length($_);
			if ($_=~/.*N{5,}.*/){$c_n_seq++;}
			$tag=0;
		}
	}
	close $fq;
	my $store_raw_n=$c_n;
	my $store_raw_bp=$c_bp;
	print $s1 "$code\t$store_raw_n\t$store_raw_bp\t$c_n_seq\t$file\n";
}
close $s1;
