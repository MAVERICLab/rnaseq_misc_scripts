#!/usr/bin/perl
use strict;
use autodie;
use lib '/home/simroux/Utiles/Perl_Modules/';
use Custom::Utils;
use Bio::SeqIO;
use Getopt::Long;
my $h='';
my $bam_dir='';
GetOptions ('help' => \$h, 'h' => \$h, 'b=s'=>\$bam_dir);
if ($h==1 || $bam_dir eq ""){ # If asked for help or did not set up any argument
	print "# Script to count mapped reads in a directory
# Arguments : 
# -b : bam dir\n";
	die "\n";
}

my $th_mismatch=1;

# Get libs info
print "Gathering libraries information ...\n";
my %info;
my %name_to_id;
my $j=1;
my @tab_f=<Libraries_Id/*.txt>;
foreach my $file (@tab_f){
	open my $tsv,"<",$file;
	my %c_name;
	my $tag=0;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^1=libraryName/){
			$tag=1;
			my @tab=split("\t",$_);
			for (my $i=0;$i<=$#tab;$i++){
				if ($tab[$i]=~/\d+=(.*)/){
					$c_name{$i}=$1;
				}
				else{
					print "Pblm with $tab[$i]\n";
				}
			}
		}
		elsif($tag==1){
			my @tab=split("\t",$_);
			$info{$tab[0]}{"order"}+=$j;
			$j++;
			for (my $i=1;$i<=$#tab;$i++){
				$tab[$i]=~s/\s/\_/g;
				$info{$tab[0]}{$c_name{$i}}=$tab[$i];
				if ($c_name{$i} eq "sampleName"){
					if ($tab[$i]=~/.*(\d)([A-D])/){
						$info{$tab[0]}{"time_point"}=$1;
						$info{$tab[0]}{"replicate"}=$2;
					}
				}
			}
		}
	}
	close $tsv;
}


my @tab_dir=split(",",$bam_dir);
my %store;
foreach my $dir (@tab_dir){
	my @list=<$dir/*.bam>;
# 	@list=($list[0]);
	foreach my $file (@list){
		$file=~/.*\/([^\/]+)_hits\.bam/;
		my $sample=$1;
		print "Reading $file - $sample ...\n";
		my $n_unmapped=`samtools view -f 4 $file | wc -l`;
		chop($n_unmapped);
		my $n_mapped=0;
		my $n_bad_mapped=0;
		open my $bam,"samtools view -F 4 $file |";
		my $i=0;
		my $max=0;
		while(<$bam>){
			chomp($_);
			my @tab=split("\t",$_);
			if ($tab[2] eq "*"){
				$n_unmapped++;
				print "This should never happen\n";
				<STDIN>;
			}
			else{
				my $tags=join(" ",@tab[11..$#tab]);
				if ($tags=~/NM:i:(\d+)/){
					if ($1<=$th_mismatch){
						$n_mapped++
					}
					else{
						$n_bad_mapped++;
					}
				}
				else{
					print "No NM for a mapped read ??\n";
					print "$_\n";
					for (my $i=0;$i<=$#tab;$i++){
						print "$i => $tab[$i]\n";
					}
					print "tags - $tags\n";
					<STDIN>;
				}
			}
		}
		close $bam;
		$store{$sample}{"m"}=$n_mapped;
		$store{$sample}{"u"}=$n_unmapped;
		$store{$sample}{"b"}=$n_bad_mapped;
	}
}

print "Library,Time point,Replicate,# Filtered reads,# reads mapped,# reads unmapped,# reads mapped with more than $th_mismatch mismatch\n";
foreach my $sample (sort {$info{$a}{"sampleName"} cmp $info{$b}{"sampleName"}} keys %store){
	my $name=$info{$sample}{"sampleName"};
	print "$name,$info{$sample}{time_point},$info{$sample}{replicate},$info{$sample}{filteredReads},$store{$sample}{m},$store{$sample}{u},$store{$sample}{b}\n";
}