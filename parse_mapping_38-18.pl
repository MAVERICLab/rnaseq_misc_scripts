#!/usr/bin/perl
use strict;
use lib '/home/simroux/Utiles/Perl_Modules/';
use Parallel::ForkManager;
use Bio::SeqIO;
# Script to parse the mapping of RNA Seq sequences to a genome
# Argument 0 : mapping files directory
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[2])))
{
	print "# Script to parse the mapping of RNA Seq with strand specificity sequences to a genome
# Argument 0 : mapping files directory
# Argument 1 : Output directory
# Argument 2 : Threshold\n";
	die "\n";
}

my $sam_dir=$ARGV[0];
my $out_dir=$ARGV[1];
my $th_quality_mapping=$ARGV[2];
print "Mapping threshold will be $th_quality_mapping\n";
my @liste_mapping=<$sam_dir*.sam>;
my %liste_conditions;
my %store;
my %store_counts;

my @tab_gene_phage;
my @tab_gene_host;

my %infos;
my %map_host;
my $gb_host="Ref_files/141118_Cbal18_PreReleaseGB_4Mb.gb";
my $seqio_object = Bio::SeqIO->new(-file => "<$gb_host", -verbose => -1 );
while (my $seq=$seqio_object->next_seq){
	my $id=$seq->id;
	my $length=length($seq->seq());
	print "Host genome is $length long\n";
	for my $feat_object ( $seq->get_SeqFeatures) {
		if ($feat_object->primary_tag eq "tRNA") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$product=~s/,//g;
			push(@tab_gene_host,$tag);
			$infos{"host"}{$tag}{"start"}=$start;
			$infos{"host"}{$tag}{"stop"}=$stop;
			$infos{"host"}{$tag}{"length"}=$stop-$start;
			$infos{"host"}{$tag}{"strand"}=$strand;
			$infos{"host"}{$tag}{"function"}=$product;
			$infos{"host"}{$tag}{"type"}="tRNA";
			for (my $i=$start;$i<=$stop;$i++){
				$map_host{$i}{$tag}="tRNA";
			}
		}
		if ($feat_object->primary_tag eq "rRNA") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$product=~s/,//g;
			print "$tag is an rRNA - $product\n";
			push(@tab_gene_host,$tag);
			$infos{"host"}{$tag}{"start"}=$start;
			$infos{"host"}{$tag}{"stop"}=$stop;
			$infos{"host"}{$tag}{"length"}=$stop-$start;
			$infos{"host"}{$tag}{"strand"}=$strand;
			$infos{"host"}{$tag}{"function"}=$product;
			$infos{"host"}{$tag}{"type"}="rRNA";
			for (my $i=$start;$i<=$stop;$i++){
				$map_host{$i}{$tag}="rRNA";
			}
		}
		if ($feat_object->primary_tag eq "CDS") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$product=~s/,//g;
			push(@tab_gene_host,$tag);
			$infos{"host"}{$tag}{"start"}=$start;
			$infos{"host"}{$tag}{"stop"}=$stop;
			$infos{"host"}{$tag}{"length"}=$stop-$start;
			$infos{"host"}{$tag}{"strand"}=$strand;
			$infos{"host"}{$tag}{"function"}=$product;
			$infos{"host"}{$tag}{"type"}="CDS";
			for (my $i=$start;$i<=$stop;$i++){
				$map_host{$i}{$tag}="CDS";
			}
		}
		if ($feat_object->primary_tag eq "ncRNA") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$product=~s/,//g;
			push(@tab_gene_host,$tag);
			$infos{"host"}{$tag}{"start"}=$start;
			$infos{"host"}{$tag}{"stop"}=$stop;
			$infos{"host"}{$tag}{"length"}=$stop-$start;
			$infos{"host"}{$tag}{"strand"}=$strand;
			$infos{"host"}{$tag}{"function"}=$product;
			$infos{"host"}{$tag}{"type"}="ncRNA";
			for (my $i=$start;$i<=$stop;$i++){
				$map_host{$i}{$tag}="ncRNA";
			}
		}
		
	}
}



# my $host_function="Ref_files/Cba18_function.tab";
# my %map_host;
# open(FU,"<$host_function") || die ("pblm opening file $host_function\n");
# while (<FU>){
# 	chomp($_);
# 	my @tab=split("\t",$_);
# 	if ($tab[3]=~/^(cb[^_]+_[^_]+)_(\d+)_(\d+)/){
# 		my $contig=$1;
# 		my $start=$2;
# 		my $stop=$3;
# 		if ($stop<$start){
# 			my $t=$start;
# 			$start=$stop;
# 			$stop=$t;
# 		}
# 		my $code=1;
# 		if ($tab[7]=~/^\d+S RNA;/){
# # 			print "$_ est un ARN Ribosomal\n";
# 			$code=2;
# 		}
# 		for (my $i=$start;$i<=$stop;$i++){
# 			$map_host{$contig}{$i}{$tab[0]}=$code;
# 		}
# 		my $strand=$tab[6];
# 		my $function=$tab[7];
# 		push(@tab_gene_host,$tab[0]);
# 		$infos{"host"}{$tab[0]}{"contig"}=$contig;
# 		$infos{"host"}{$tab[0]}{"start"}=$start;
# 		$infos{"host"}{$tab[0]}{"stop"}=$stop;
# 		$infos{"host"}{$tab[0]}{"length"}=$stop-$start;
# 		$infos{"host"}{$tab[0]}{"strand"}=$strand;
# 		$function=~s/,//g;
# 		$infos{"host"}{$tab[0]}{"function"}=$function;
# # 		print "CDS - $tab[0] from $start to $stop on strand $strand\n";
# 	}
# 	else{
# 		print "$tab[3] weird\n";
# 	}
# }
# close FU;

##
##
##
# my $gb_phage="Ref_files/Cba_phage_38.gb";
## HERE A SPECIAL FOR UAGC
my $gb_phage="Ref_files/141114_phi381_start33820bp.gb";
##
##
##
my %map_phage;
my $max_phage=0;
my $seqio_object = Bio::SeqIO->new(-file => "<$gb_phage", -verbose => -1 );
while (my $seq=$seqio_object->next_seq){
	my $id=$seq->id;
	my $length=length($seq->seq());
	print "Phage genome is $length long\n";
	$max_phage=$length;
	for my $feat_object ( $seq->get_SeqFeatures) {
		if ($feat_object->primary_tag eq "tRNA") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$infos{"phage"}{$tag}{"contig"}=$id;
			$infos{"phage"}{$tag}{"start"}=$start;
			$infos{"phage"}{$tag}{"stop"}=$stop;
			$infos{"phage"}{$tag}{"strand"}=$strand;
			$infos{"phage"}{$tag}{"length"}=$stop-$start;
			$product=~s/,//g;
			$infos{"phage"}{$tag}{"function"}=$product;
# 			print "CDS - $tag from $start to $stop on strand $strand\n";
			for (my $i=$start;$i<=$stop;$i++){
# 				if (defined($map{$i})){print "!! overlap on $i - $tag / $map{$i}\n";}
				$map_phage{$i}{$tag}=2;
			}
			push(@tab_gene_phage,$tag);
		}
		if ($feat_object->primary_tag eq "CDS") {
			my @l_tag=$feat_object->get_tag_values('locus_tag');
			my $tag=$l_tag[0];
			my $start=$feat_object->start;
			my $stop=$feat_object->end;
			my $strand=$feat_object->strand;
			if ($strand eq "1"){$strand="+"}
			else{$strand="-"}
			my @l_function=$feat_object->get_tag_values('product');
			my $product=$l_function[0];
			$infos{"phage"}{$tag}{"contig"}=$id;
			$infos{"phage"}{$tag}{"start"}=$start;
			$infos{"phage"}{$tag}{"stop"}=$stop;
			$infos{"phage"}{$tag}{"strand"}=$strand;
			$infos{"phage"}{$tag}{"length"}=$stop-$start;
			$product=~s/,//g;
			$infos{"phage"}{$tag}{"function"}=$product;
# 			print "CDS - $tag from $start to $stop on strand $strand\n";
			for (my $i=$start;$i<=$stop;$i++){
# 				if (defined($map{$i})){print "!! overlap on $i - $tag / $map{$i}\n";}
				$map_phage{$i}{$tag}=1;
			}
			push(@tab_gene_phage,$tag);
		}
		if ($feat_object->primary_tag eq "ncRNA") {
# 			my @l_tag=$feat_object->get_tag_values('locus_tag');
# 			my $tag=$l_tag[0];
# 			my $start=$feat_object->start;
# 			my $stop=$feat_object->end;
# 			my $strand=$feat_object->strand;
# 			if ($strand eq "1"){$strand="+"}
# 			else{$strand="-"}
# 			my @l_function=$feat_object->get_tag_values('product');
# 			my $product=$l_function[0];
# 			$infos{"phage"}{$tag}{"contig"}=$id;
# 			$infos{"phage"}{$tag}{"start"}=$start;
# 			$infos{"phage"}{$tag}{"stop"}=$stop;
# 			$infos{"phage"}{$tag}{"strand"}=$strand;
# 			$infos{"phage"}{$tag}{"length"}=$stop-$start;
# 			$product=~s/,//g;
# 			$infos{"phage"}{$tag}{"function"}=$product;
# # 			print "ncRNA - $tag from $start to $stop on strand $strand\n";
# 			for (my $i=$start;$i<=$stop;$i++){
# # 				if (defined($map{$i})){print "!! overlap on $i - $tag / $map{$i}\n";}
# 				$map_phage{$i}{$tag}=1;
# 			}
# 			push(@tab_gene_phage,$tag);
		}
		
	}
}
# <STDIN>;

my $th_noncoding = 20;
my $n_processes = 6;
my $pm = Parallel::ForkManager->new( $n_processes );
foreach(sort @liste_mapping){
	my $mapping_file=$_;
	$mapping_file=~/.*\/(.*)\.sam/;
	my $id=$1;
	my @t=split("-vs-",$id);
	my $condition=$t[0];
	$liste_conditions{$condition}=1;
	print "Processing $mapping_file ... $id\n";
	$pm->start and next;
	if ($t[1]=~/.*Phi.*/){
		Mapping phage
		print "We look at $mapping_file which map reads on the phage\n";
		my $n=0;
		my %store_counts;
		my %store_non_coding;
		my %store_coverage;
		my %store_coverage_ss;
		my %store_coverage_ss_noncoding;
		my $count=0;
		open (MAP,"<$mapping_file") || die ("pblm opening file $mapping_file\n");
		while (<MAP>){
			chomp($_);
			if ($_=~/^\@/){}
			else{
				my @tab=split("\t",$_);
				my $read=$tab[0];
		# 		my $contig=$tab[2];
				my $bitwise_code=$tab[1];
				my $strand="+";
				if ($bitwise_code & 16){
					$strand="-";
				}
				my $seq_read=$tab[9];
				my $length=length($seq_read);
				my $start=$tab[3]-1;
				my $quality=$tab[4];
				if ($quality>$th_quality_mapping){
					my $tag=0;
					my %list;
					$n++;
					for(my $i=$start;$i<=$start+$length;$i++){
						$store_coverage_ss{$strand}{$i}++;
						if (defined($map_phage{$i})){
							my $tag2=0;
							foreach(keys %{$map_phage{$i}}){
								if ($infos{"phage"}{$_}{"strand"} eq $strand){
									$list{$_}=1;
									$tag2=1;
								}
							}
							if($tag2==0){
								$store_non_coding{$i}++;
								$store_coverage_ss_noncoding{$strand}{$i}++;
							}
							else{
								$store_coverage{$i}++;
							}
						}
						else{
							$store_non_coding{$i}++;
							$store_coverage_ss_noncoding{$strand}{$i}++;
						}
					}
# 					my @tab_temp=keys %list;
# 					if ($#tab_temp>=0){$count++;}
					foreach(keys %list){
						$store_counts{$condition}{"phage"}{$_}++;
					}
				}
			}
		}
		close MAP;
# 		$store{$condition}{"phage"}=$n;
		print "$condition mapped on phage -> $n reads\n";
		my $out_file_2=$out_dir.$condition."_vs_phage.csv";
		open(S2,">$out_file_2") || die ("pblm opening file $out_file_2\n");
		print S2 "#Gene-ID,Chr,Strand,Start,End,Count,Length,Transcripts\n";
		foreach(@tab_gene_phage){
			if (!defined($store_counts{$condition}{"phage"}{$_})){$store_counts{$condition}{"phage"}{$_}=0;}
			print S2 "$_,$infos{phage}{$_}{contig},$infos{phage}{$_}{strand},$infos{phage}{$_}{start},$infos{phage}{$_}{stop},$store_counts{$condition}{phage}{$_},$infos{phage}{$_}{length},$infos{phage}{$_}{function}\n";
		}
		close S2;
		my $out_file_3=$out_dir.$condition."_vs_phage_noncoding.csv";
		open(S3,">$out_file_3") || die ("pblm opening file $out_file_3\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_non_coding{$i})){
				print S3 "$store_non_coding{$i}\n";
			}
			else{print S3 "0\n";}
		}
		close S3;
		
		my $out_file_4=$out_dir.$condition."_vs_phage_CDS_coverage.csv";
		open(S4,">$out_file_4") || die ("pblm opening file $out_file_4\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_coverage{$i})){
				print S4 "$store_coverage{$i}\n";
			}
			else{print S4 "0\n";}
		}
		close S4;
		my $out_file_5=$out_dir.$condition."_vs_phage_coverage-plus-strand.csv";
		open(S5,">$out_file_5") || die ("pblm opening file $out_file_5\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_coverage_ss{"+"}{$i})){
				print S5 $store_coverage_ss{"+"}{$i}."\n";
			}
			else{print S5 "0\n";}
		}
		close S5;
		my $out_file_6=$out_dir.$condition."_vs_phage_coverage-minus-strand.csv";
		open(S6,">$out_file_6") || die ("pblm opening file $out_file_6\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_coverage_ss{"-"}{$i})){
				print S6 $store_coverage_ss{"-"}{$i}."\n";
			}
			else{print S6 "0\n";}
		}
		close S6;
		my $out_file_7=$out_dir.$condition."_vs_phage_coverage-non_coding-plus-strand.csv";
		open(S7,">$out_file_7") || die ("pblm opening file $out_file_7\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_coverage_ss_noncoding{"+"}{$i})){
				print S7 $store_coverage_ss_noncoding{"+"}{$i}."\n";
			}
			else{print S7 "0\n";}
		}
		close S7;
		my $out_file_8=$out_dir.$condition."_vs_phage_coverage-non_coding-minus-strand.csv";
		open(S8,">$out_file_8") || die ("pblm opening file $out_file_8\n");
		for(my $i=1;$i<=$max_phage;$i++){
			if (defined($store_coverage_ss_noncoding{"-"}{$i})){
				print S8 $store_coverage_ss_noncoding{"-"}{$i}."\n";
			}
			else{print S8 "0\n";}
		}
		close S8;
	}
	else{
		# Mapping host
		print "We look at $mapping_file which map reads on the host\n";
		my $n=0;
		my $total_not_rRNA=0;
		my $total_rRNA=0;
		my $total_non_coding=0;
		my %store_counts;
		my %store_non_coding;
		open (MAP,"<$mapping_file") || die ("pblm opening file $mapping_file\n");
		while (<MAP>){
			chomp($_);
			if ($_=~/^\@/){}
			else{
				my @tab=split("\t",$_);
				my $read=$tab[0];
				my $bitwise_code=$tab[1];
				my $strand="+";
				if ($bitwise_code & 16){
					$strand="-";
				}
				my $contig=$tab[2];
				my $seq_read=$tab[9];
				my $length=length($seq_read);
				my $start=$tab[3]-1;
				my $quality=$tab[4];
				my %list;
				if ($quality>$th_quality_mapping){
# 					print "$read mapped\n";
					my $tag=0;
					$n++;
					for(my $i=$start;$i<=$start+$length;$i++){
						if (defined($map_host{$i})){
# 							print "There is an annotation on position $i\n";
							my $tag2=0;
							foreach(keys %{$map_host{$i}}){
								if ($infos{"host"}{$_}{"strand"} eq $strand){
									$list{$_}=1;
# 									print "$_ comes with us !\n";
									if ($map_host{$i}{$_} eq "rRNA"){
# 										print "\t oooh a rRNA !\n";
										$tag=1;
									}
								}
								else{
# 									print "but $_ is not on the good strand $strand\n";
								}
							}
							if($tag2==0){
								$store_non_coding{$i}++;
							}
						}
					}
					if ($tag==1){$total_rRNA++;}
					else{
						my @tab_temp=keys %list;
						if ($#tab_temp>=0){	
							$total_not_rRNA++;
# 							print "So we add a read matching not to a rRNA\n";
							foreach(keys %list){
								$store_counts{$condition}{"host"}{$_}++;
							}
						}
						else{
# 							print "So we have a $read read that mapped, but not to a known gene ????\n";
							$total_non_coding++;
						}
					}
				}
			}
		}
		close MAP;
# 		$store{$condition}{"host"}=$total_not_rRNA;
# 		$store{$condition}{"rrna"}=$total_rRNA;
		my $out_file_1=$out_dir.$condition."_vs_host.csv";
		print "\t$out_file_1 - $n mapping on host -> $total_rRNA on rDNA / $total_not_rRNA on other CDS / $total_non_coding non coding\n";
		open(S1,">$out_file_1") || die ("pblm opening file $out_file_1\n");
		print S1 "#Gene-ID,Chr,Strand,Start,End,Count,Length,Transcripts\n";
		foreach(@tab_gene_host){
			if (!defined($store_counts{$condition}{"host"}{$_})){$store_counts{$condition}{"host"}{$_}=0;}
			print S1 "$_,$infos{host}{$_}{contig},$infos{host}{$_}{strand},$infos{host}{$_}{start},$infos{host}{$_}{stop},$store_counts{$condition}{host}{$_},$infos{host}{$_}{length},$infos{host}{$_}{function}\n";
		}
		close S1;
		my $out_file_3=$out_dir.$condition."_vs_host_noncoding.csv";
		open(S3,">$out_file_3") || die ("pblm opening file $out_file_3\n");
		print S3 "#Position,No_reads_mapped\n";
		foreach(sort { $a <=> $b } keys %store_non_coding){
			my $pos=$_;
			if($pos>$th_noncoding){
				print S3 "$_,$store_non_coding{$_}\n";
			}
		}
		close S3;
	}
	$pm->finish;
}
$pm->wait_all_children;
# print "Sample\tHost\tHost rDNA\tPhage\n";
# foreach(sort keys %store){
# 	my $cond=$_;
# 	print "$cond\t$store{$cond}{host}\t$store{$cond}{rdna}\t$store{$cond}{phage}\n";
# }

# And then two global csv files with counts for everyone
### NOW WITH A SEPARATE SCRIPT ==> TO WRITE
# my @tab_conditions=sort keys %liste_conditions;
# my $out_global_1=$out_dir."Global_vs_host.csv";
# open(S1,">$out_global_1") || die ("pblm opening file $out_global_1\n");
# my $first_line="Gene,".join(",",@tab_conditions);
# print S1 "$first_line\n";
# foreach(@tab_gene_host){
# 	my $gene=$_;
# 	my $l="$gene";
# 	foreach(@tab_conditions){
# 		if (defined($store_counts{$_}{"host"}{$gene})){$l.=",$store_counts{$_}{host}{$gene}"}
# 		else{$l.=",0";}
# 	}
# 	print S1 "$l\n";
# }
# close S1;
# my $out_global_2=$out_dir."Global_vs_phage.csv";
# open(S2,">$out_global_2") || die ("pblm opening file $out_global_2\n");
# my $first_line="Gene,".join(",",@tab_conditions);
# print S2 "$first_line\n";
# foreach(@tab_gene_phage){
# 	my $gene=$_;
# 	my $l="$gene";
# 	foreach(@tab_conditions){
# 		if (defined($store_counts{$_}{"phage"}{$gene})){$l.=",$store_counts{$_}{phage}{$gene}"}
# 		else{$l.=",0";}
# 	}
# 	print S2 "$l\n";
# }
# close S2;
