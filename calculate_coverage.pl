#!/usr/bin/perl
use strict;
use autodie;
use Getopt::Long;
use Custom::Utils;
my $h='';
my $count_file='';
my $gene_table='';
GetOptions ('help' => \$h, 'h' => \$h, 'i=s'=>\$count_file, 't=s'=>\$gene_table);
if ($h==1 || $count_file eq "" || $gene_table eq ""){ # If asked for help or did not set up any argument
	print "# Script to calculate average genome coverage from gene counts
# Arguments : 
-i : gene counts
-t : ref table of genes from genome\n";
	die "\n";	
}

# Get libs info
print "Gathering libraries information ...\n";
my %info;
my %name_to_id;
my $j=1;
my @tab_f=<Libraries_Id/*.txt>;
foreach my $file (@tab_f){
	open my $tsv,"<",$file;
	my %c_name;
	my $tag=0;
	while(<$tsv>){
		chomp($_);
		if ($_=~/^1=libraryName/){
			$tag=1;
			my @tab=split("\t",$_);
			for (my $i=0;$i<=$#tab;$i++){
				if ($tab[$i]=~/\d+=(.*)/){
					$c_name{$i}=$1;
				}
				else{
					print "Pblm with $tab[$i]\n";
				}
			}
		}
		elsif($tag==1){
			my @tab=split("\t",$_);
			$info{$tab[0]}{"order"}+=$j;
			$j++;
			for (my $i=1;$i<=$#tab;$i++){
				$tab[$i]=~s/\s/\_/g;
				$info{$tab[0]}{$c_name{$i}}=$tab[$i];
				if ($c_name{$i} eq "sampleName"){
					$name_to_id{$tab[$i]}=$tab[0];
					if ($tab[$i]=~/.*(\d)([A-D])/){
						$info{$tab[0]}{"time_point"}=$1;
						$info{$tab[0]}{"replicate"}=$2;
					}
				}
			}
		}
	}
	close $tsv;
}

my %store;
my %c_name;
my %check_sample;
my %count;
my $tag=0;
print "Reading $count_file\n";
open my $csv,"<",$count_file;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tag==0){
		$tag=1;
		for (my $i=1;$i<=$#tab;$i++){
			$check_sample{$tab[$i]}=1;
			$c_name{$i}=$tab[$i];
		}
	}
	else{
		for (my $i=1;$i<=$#tab;$i++){
			$store{$tab[0]}{$c_name{$i}}+=$tab[$i];
		}
	}
}
close $csv;

my %info_gene;
print "Gathering genes info ...\n";
open my $csv,"<",$gene_table;
while(<$csv>){
	chomp($_);
	my @tab=split(",",$_);
	if ($tab[0] eq "Gene"){next;}
	$info_gene{$tab[0]}{"length"}=$tab[5]-$tab[4];
	my $start=$tab[4];
	my $stop=$tab[5];
	my $strand="forward";
	$info_gene{$tab[0]}{"start"}=$start;
	$info_gene{$tab[0]}{"stop"}=$stop;
	$info_gene{$tab[0]}{"function"}=$tab[2];
}
close $csv;

my $read_len=100;
print "Prepping the output\n";
print "Sample\tAverage coverage\tMinimum coverage\tMaximum coverage\n";
foreach my $sample (sort keys %check_sample){
	my $avg=0;
	my $n=0;
	my $min=99999;
	my $max=-10;
	foreach my $gene (sort keys %info_gene){
		$n++;
		my $cover=$store{$gene}{$sample}*$read_len/$info_gene{$gene}{"length"};
		$avg+=$cover;
		if ($cover>$max){$max=$cover;}
		if ($cover<$min){$min=$cover;}
	}
	$avg/=$n;
	print "$sample\t$avg\t$min\t$max\n";
}